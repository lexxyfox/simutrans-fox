import argparse

FLAG = {'action': 'store_true'}

parser = argparse.ArgumentParser(prog='simutrans', description='Simutrans Extended v120.4.1')
parser.add_argument('-announce', **FLAG)
parser.add_argument('-compare')
parser.add_argument('-easyserver')
parser.add_argument('-server')
parser.add_argument('-server_altdns')
parser.add_argument('-server_dns')
parser.add_argument('-server_name')
parser.add_argument('-theme')

group3 = parser.add_argument_group('General arguments')
group3.add_argument('--datadir', metavar='DATA_DIR', help='Sets DATA_DIR. This is where simutrans looks for fonts, scripts, language files ("text"), and themes. Defaults to /usr/share/games/simutrans-ex/')
group3.add_argument('--debug', metavar='LVL', help='Set logging verbosity to LVL. Debugging messages are sent to stderr. Must be between 0-9. Default: 0')
group3.add_argument('--lang', metavar = 'CODE', help='Loads the language pack from DATA_DIR/text/CODE.tab.')
group3.add_argument('--load', metavar='NAME', help='Loads the savegame from USER_DIR/save/NAME.sve. If NAME is in the format of "net:HOST" or "net:HOST:PORT", simutrans will attempt to connect to the game server located at HOST:PORT. PORT defaults to 13353.')
group3.add_argument('--noaddons', **FLAG, help='Disable loading additional pak data from USER_DIR/addons.')
group3.add_argument('--pause', **FLAG, help='In singleplayer mode, will start the game paused. With -server, will cause the server to pause when no clients are connected.')
group3.add_argument('--run-in-background', **FLAG, help='Allow background tasks to run while game is paused.')
group3.add_argument('--threads', metavar='NUM', help='Use up to NUM computing thread(s). Default: 1')
group3.add_argument('--until', metavar = 'MONTH', help = 'Causes the game to quit after MONTH. Quits immediately if MONTH has already past.')
group3.add_argument('--userdir', metavar='USER_DIR', help='Sets USER_DIR. This is where simutrans looks for addons, maps, save files, user settings, and stores screenshots. Defaults to $HOME/simutrans')
group3.add_argument('--objects', metavar='PAK_DIR', help='Pak files are loaded from DATA_DIR/PAK_DIR. Additionally, any paks or config settings in USER_DIR/addons will override those from this directory unless --noaddons is specified.')

gameplay = parser.add_argument_group('Gameplay settings')
gameplay.add_argument('--freeplay', **FLAG, help='Play with unlimited money.')
gameplay.add_argument('--startyear')
gameplay.add_argument('--timeline')

group2 = parser.add_argument_group('Graphics settings')
group2.add_argument('--autodpi', **FLAG, help = 'Attempt to automatically scale the game to match an equivalent of 96 PPI.')
group2.add_argument('--fullscreen', **FLAG, help='Starts the game in fullscreen mode.')
group2.add_argument('--use-hw', **FLAG, help = 'Enable hardware double buffering.')
group2.add_argument('--vsync', **FLAG, help='Enable VSync.')
group2.add_argument('--windowsize', metavar='WxH', help='Sets the screen size to width W and height H.')

audio = parser.add_argument_group('Audio settings')
audio.add_argument('--nosound', **FLAG, help='Starts the game with sound effects disabled.')
audio.add_argument('--nomusic', **FLAG, help='Starts the game with music disabled.')
audio.add_argument('--mute', **FLAG, help='Alias for -nosound --nomusic')

group1 = parser.add_argument_group('Debugging arguments')
group1.add_argument('--benchmark', **FLAG, help = 'Performs some simple profiling of drawing routines after initilizing the game. Works best with --load and --until.')
group1.add_argument('--sizes', **FLAG, help='Prints the in-memory sizes of several internal structures. --debug must be at least 3.')
group1.add_argument('--heavy', metavar='LVL', help='Determines how much data is syned between client and server in multiplayer mode. Useful for stressing netork connection. LVL must be 0 (default), 1, or 2. ')

args = parser.parse_args()
print(args.accumulate(args.integers))
