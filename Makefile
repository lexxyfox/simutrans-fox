srcdir ?= simutrans
BUILDDIR ?= build

# SDL_VIDEO_X11_WMCLASS='Simutrans Extended'

export QUILT_DIFF_ARGS := -p ab --no-timestamps --no-index --sort
export QUILT_REFRESH_ARGS := -p ab --no-timestamps --no-index --sort

export LD_LIBRARY_PATH := $(BUILDDIR)

#STRIP_CMD = strip -DdgMSsw --remove-section=.comment --remove-section=.note* '$@'
STRIP_CMD = strip -s --remove-section=.comment '$@'
QUILT_CMD = quilt --quiltrc .quiltrc

# g++ flags for shared objects (*.so)
SOFLAGS ?= -fpic -shared
# g++ doesn't support zstd compression above 19 for some reason
LTOFLAGS ?= -flto=auto -flto-compression-level=19

CXXFLAGS += \
	-s \
	-std=gnu++11 \
	-Ofast \
	-pipe \
	-pthread \
	-Wno-unused-result \
	-Wl,--copy-dt-needed-entries \
	-L'$(BUILDDIR)' \
	-DHAS_64_BIT_SYSTEM \
	-DMSG_LEVEL=3 \
	-DMULTI_THREAD \
	-DNDEBUG \
	-DSYSLOG \
	-DUSE_UPNP \
	-DUSE_ZSTD

SOURCES_BASE += \
	$(srcdir)/bauer/brueckenbauer.cc \
	$(srcdir)/bauer/fabrikbauer.cc \
	$(srcdir)/bauer/goods_manager.cc \
	$(srcdir)/bauer/hausbauer.cc \
	$(srcdir)/bauer/pier_builder.cc \
	$(srcdir)/bauer/tree_builder.cc \
	$(srcdir)/bauer/tunnelbauer.cc \
	$(srcdir)/bauer/vehikelbauer.cc \
	$(srcdir)/bauer/wegbauer.cc \
	$(srcdir)/boden/boden.cc \
	$(srcdir)/boden/brueckenboden.cc \
	$(srcdir)/boden/fundament.cc \
	$(srcdir)/boden/monorailboden.cc \
	$(srcdir)/boden/pier_deck.cc \
	$(srcdir)/boden/tunnelboden.cc \
	$(srcdir)/boden/wasser.cc \
	$(srcdir)/boden/wege/kanal.cc \
	$(srcdir)/boden/wege/maglev.cc \
	$(srcdir)/boden/wege/monorail.cc \
	$(srcdir)/boden/wege/narrowgauge.cc \
	$(srcdir)/boden/wege/runway.cc \
	$(srcdir)/boden/wege/schiene.cc \
	$(srcdir)/boden/wege/strasse.cc \
	$(srcdir)/boden/wege/weg.cc \
	$(srcdir)/convoy.cc \
	$(srcdir)/dataobj/crossing_logic.cc \
	$(srcdir)/dataobj/gameinfo.cc \
	$(srcdir)/dataobj/height_map_loader.cc \
	$(srcdir)/dataobj/koord3d.cc \
	$(srcdir)/dataobj/koord.cc \
	$(srcdir)/dataobj/livery_scheme.cc \
	$(srcdir)/dataobj/marker.cc \
	$(srcdir)/dataobj/objlist.cc \
	$(srcdir)/dataobj/powernet.cc \
	$(srcdir)/dataobj/rect.cc \
	$(srcdir)/dataobj/replace_data.cc \
	$(srcdir)/dataobj/ribi.cc \
	$(srcdir)/dataobj/route.cc \
	$(srcdir)/dataobj/scenario.cc \
	$(srcdir)/dataobj/schedule.cc \
	$(srcdir)/dataobj/tabfile.cc \
	$(srcdir)/dataobj/translator.cc \
	$(srcdir)/descriptor/bridge_desc.cc \
	$(srcdir)/descriptor/building_desc.cc \
	$(srcdir)/descriptor/factory_desc.cc \
	$(srcdir)/descriptor/goods_desc.cc \
	$(srcdir)/descriptor/obj_base_desc.cc \
	$(srcdir)/descriptor/pier_desc.cc \
	$(srcdir)/descriptor/reader/bridge_reader.cc \
	$(srcdir)/descriptor/reader/building_reader.cc \
	$(srcdir)/descriptor/reader/citycar_reader.cc \
	$(srcdir)/descriptor/reader/crossing_reader.cc \
	$(srcdir)/descriptor/reader/factory_reader.cc \
	$(srcdir)/descriptor/reader/good_reader.cc \
	$(srcdir)/descriptor/reader/groundobj_reader.cc \
	$(srcdir)/descriptor/reader/ground_reader.cc \
	$(srcdir)/descriptor/reader/imagelist2d_reader.cc \
	$(srcdir)/descriptor/reader/imagelist3d_reader.cc \
	$(srcdir)/descriptor/reader/imagelist_reader.cc \
	$(srcdir)/descriptor/reader/obj_reader.cc \
	$(srcdir)/descriptor/reader/pedestrian_reader.cc \
	$(srcdir)/descriptor/reader/pier_reader.cc \
	$(srcdir)/descriptor/reader/roadsign_reader.cc \
	$(srcdir)/descriptor/reader/root_reader.cc \
	$(srcdir)/descriptor/reader/sim_reader.cc \
	$(srcdir)/descriptor/reader/skin_reader.cc \
	$(srcdir)/descriptor/reader/sound_reader.cc \
	$(srcdir)/descriptor/reader/text_reader.cc \
	$(srcdir)/descriptor/reader/tree_reader.cc \
	$(srcdir)/descriptor/reader/tunnel_reader.cc \
	$(srcdir)/descriptor/reader/vehicle_reader.cc \
	$(srcdir)/descriptor/reader/way_obj_reader.cc \
	$(srcdir)/descriptor/reader/way_reader.cc \
	$(srcdir)/descriptor/reader/xref_reader.cc \
	$(srcdir)/descriptor/sound_desc.cc \
	$(srcdir)/descriptor/tunnel_desc.cc \
	$(srcdir)/descriptor/vehicle_desc.cc \
	$(srcdir)/descriptor/way_desc.cc \
	$(srcdir)/display/viewport.cc \
	$(srcdir)/finder/placefinder.cc \
	$(srcdir)/freight_list_sorter.cc \
	$(srcdir)/gui/base_info.cc \
	$(srcdir)/gui/baum_edit.cc \
	$(srcdir)/gui/citybuilding_edit.cc \
	$(srcdir)/gui/climates.cc \
	$(srcdir)/gui/components/gui_aligned_container.cc \
	$(srcdir)/gui/components/gui_building.cc \
	$(srcdir)/gui/components/gui_button_to_chart.cc \
	$(srcdir)/gui/components/gui_component.cc \
	$(srcdir)/gui/components/gui_container.cc \
	$(srcdir)/gui/components/gui_image.cc \
	$(srcdir)/gui/components/gui_image_list.cc \
	$(srcdir)/gui/components/gui_map_preview.cc \
	$(srcdir)/gui/components/gui_obj_view_t.cc \
	$(srcdir)/gui/components/gui_scrollpane.cc \
	$(srcdir)/gui/components/gui_speedbar.cc \
	$(srcdir)/gui/components/gui_table.cc \
	$(srcdir)/gui/components/gui_waytype_tab_panel.cc \
	$(srcdir)/gui/components/gui_world_view_t.cc \
	$(srcdir)/gui/convoi_filter_frame.cc \
	$(srcdir)/gui/convoi_frame.cc \
	$(srcdir)/gui/convoy_item.cc \
	$(srcdir)/gui/curiosity_edit.cc \
	$(srcdir)/gui/curiositylist_stats_t.cc \
	$(srcdir)/gui/enlarge_map_frame_t.cc \
	$(srcdir)/gui/extend_edit.cc \
	$(srcdir)/gui/factory_edit.cc \
	$(srcdir)/gui/ground_info.cc \
	$(srcdir)/gui/groundobj_edit.cc \
	$(srcdir)/gui/gui_frame.cc \
	$(srcdir)/gui/halt_list_frame.cc \
	$(srcdir)/gui/headquarter_info.cc \
	$(srcdir)/gui/help_frame.cc \
	$(srcdir)/gui/jump_frame.cc \
	$(srcdir)/gui/kennfarbe.cc \
	$(srcdir)/gui/label_info.cc \
	$(srcdir)/gui/labellist_stats_t.cc \
	$(srcdir)/gui/line_management_gui.cc \
	$(srcdir)/gui/load_relief_frame.cc \
	$(srcdir)/gui/loadsave_frame.cc \
	$(srcdir)/gui/map_frame.cc \
	$(srcdir)/gui/messagebox.cc \
	$(srcdir)/gui/message_frame_t.cc \
	$(srcdir)/gui/message_option_t.cc \
	$(srcdir)/gui/message_stats_t.cc \
	$(srcdir)/gui/obj_info.cc \
	$(srcdir)/gui/onewaysign_info.cc \
	$(srcdir)/gui/optionen.cc \
	$(srcdir)/gui/overtaking_mode.cc \
	$(srcdir)/gui/pakselector.cc \
	$(srcdir)/gui/password_frame.cc \
	$(srcdir)/gui/pier_rotation_select.cc \
	$(srcdir)/gui/privatesign_info.cc \
	$(srcdir)/gui/savegame_frame.cc \
	$(srcdir)/gui/scenario_frame.cc \
	$(srcdir)/gui/scenario_info.cc \
	$(srcdir)/gui/server_frame.cc \
	$(srcdir)/gui/settings_frame.cc \
	$(srcdir)/gui/settings_stats.cc \
	$(srcdir)/gui/signal_connector_gui.cc \
	$(srcdir)/gui/signal_info.cc \
	$(srcdir)/gui/signal_spacing.cc \
	$(srcdir)/gui/sound_frame.cc \
	$(srcdir)/gui/sprachen.cc \
	$(srcdir)/gui/station_building_select.cc \
	$(srcdir)/gui/themeselector.cc \
	$(srcdir)/gui/trafficlight_info.cc \
	$(srcdir)/gui/vehicle_class_manager.cc \
	$(srcdir)/gui/welt.cc \
	$(srcdir)/io/rdwr/adler32_stream.cc \
	$(srcdir)/network/checksum.cc \
	$(srcdir)/network/network_cmd.cc \
	$(srcdir)/network/network_cmd_ingame.cc \
	$(srcdir)/network/network_cmd_scenario.cc \
	$(srcdir)/network/network_cmp_pakset.cc \
	$(srcdir)/network/network_file_transfer.cc \
	$(srcdir)/network/network_socket_list.cc \
	$(srcdir)/network/pakset_info.cc \
	$(srcdir)/obj/baum.cc \
	$(srcdir)/obj/bruecke.cc \
	$(srcdir)/obj/crossing.cc \
	$(srcdir)/obj/field.cc \
	$(srcdir)/obj/gebaeude.cc \
	$(srcdir)/obj/groundobj.cc \
	$(srcdir)/obj/label.cc \
	$(srcdir)/obj/leitung2.cc \
	$(srcdir)/obj/pier.cc \
	$(srcdir)/obj/pillar.cc \
	$(srcdir)/obj/roadsign.cc \
	$(srcdir)/obj/signal.cc \
	$(srcdir)/obj/simobj.cc \
	$(srcdir)/obj/tunnel.cc \
	$(srcdir)/obj/wayobj.cc \
	$(srcdir)/obj/wolke.cc \
	$(srcdir)/old_blockmanager.cc \
	$(srcdir)/path_explorer.cc \
	$(srcdir)/player/ai.cc \
	$(srcdir)/player/ai_goods.cc \
	$(srcdir)/player/ai_passenger.cc \
	$(srcdir)/player/finance.cc \
	$(srcdir)/player/simplay.cc \
	$(srcdir)/script/api/api_city.cc \
	$(srcdir)/script/api/api_const.cc \
	$(srcdir)/script/api/api_control.cc \
	$(srcdir)/script/api/api_convoy.cc \
	$(srcdir)/script/api/api_factory.cc \
	$(srcdir)/script/api/api_gui.cc \
	$(srcdir)/script/api/api_halt.cc \
	$(srcdir)/script/api/api_include.cc \
	$(srcdir)/script/api/api_line.cc \
	$(srcdir)/script/api/api_map_objects.cc \
	$(srcdir)/script/api/api_obj_desc_base.cc \
	$(srcdir)/script/api/api_obj_desc.cc \
	$(srcdir)/script/api/api_player.cc \
	$(srcdir)/script/api/api_scenario.cc \
	$(srcdir)/script/api/api_schedule.cc \
	$(srcdir)/script/api/api_settings.cc \
	$(srcdir)/script/api/api_simple.cc \
	$(srcdir)/script/api/api_tiles.cc \
	$(srcdir)/script/api/api_world.cc \
	$(srcdir)/script/api_class.cc \
	$(srcdir)/script/api/export_desc.cc \
	$(srcdir)/script/api_function.cc \
	$(srcdir)/script/api/get_next.cc \
	$(srcdir)/script/api_param.cc \
	$(srcdir)/script/dynamic_string.cc \
	$(srcdir)/script/export_objs.cc \
	$(srcdir)/script/script.cc \
	$(srcdir)/simcity.cc \
	$(srcdir)/simconvoi.cc \
	$(srcdir)/simdepot.cc \
	$(srcdir)/simevent.cc \
	$(srcdir)/simfab.cc \
	$(srcdir)/simhalt.cc \
	$(srcdir)/siminteraction.cc \
	$(srcdir)/simintr.cc \
	$(srcdir)/simio.cc \
	$(srcdir)/simline.cc \
	$(srcdir)/simlinemgmt.cc \
	$(srcdir)/simmenu.cc \
	$(srcdir)/simmesg.cc \
	$(srcdir)/simplan.cc \
	$(srcdir)/simsignalbox.cc \
	$(srcdir)/simskin.cc \
	$(srcdir)/simsound.cc \
	$(srcdir)/simtool.cc \
	$(srcdir)/simunits.cc \
	$(srcdir)/simware.cc \
	$(srcdir)/simworld.cc \
	$(srcdir)/sys/clipboard_internal.cc \
	$(srcdir)/unicode.cc \
	$(srcdir)/utils/checklist.cc \
	$(srcdir)/utils/log.cc \
	$(srcdir)/utils/simrandom.cc \
	$(srcdir)/utils/simthread.cc \
	$(srcdir)/vehicle/air_vehicle.cc \
	$(srcdir)/vehicle/movingobj.cc \
	$(srcdir)/vehicle/pedestrian.cc \
	$(srcdir)/vehicle/rail_vehicle.cc \
	$(srcdir)/vehicle/road_vehicle.cc \
	$(srcdir)/vehicle/simroadtraffic.cc \
	$(srcdir)/vehicle/water_vehicle.cc \
	$(srcdir)/sys/simsys.cc \
	$(srcdir)/dataobj/environment.cc \
	$(srcdir)/utils/float32e8_t.cc \
	$(srcdir)/dataobj/loadsave.cc \
	$(srcdir)/io/classify_file.cc \
	$(srcdir)/io/rdwr/zlib_file_rdwr_stream.cc \
	$(srcdir)/io/rdwr/zstd_file_rdwr_stream.cc \
	$(srcdir)/io/rdwr/raw_file_rdwr_stream.cc \
	$(srcdir)/io/rdwr/bzip2_file_rdwr_stream.cc \
	$(srcdir)/io/rdwr/compare_file_rd_stream.cc \
	$(srcdir)/io/rdwr/rdwr_stream.cc \
	

# files needed by both server and client, but must be compiled separately
# these depend on COLOUR_DEPTH, LINEASCENT, LINESPACE, D_HEADING_HEIGHT, D_ENTRY_NO_HEIGHT, FIXED_SYMBOL_YOFF, or GOODS_COLOR_BOX_YOFF
SOURCES_GFX += \
	$(srcdir)/boden/grund.cc \
	$(srcdir)/dataobj/settings.cc \
	$(srcdir)/descriptor/ground_desc.cc \
	$(srcdir)/descriptor/image.cc \
	$(srcdir)/descriptor/reader/image_reader.cc \
	$(srcdir)/display/font.cc \
	$(srcdir)/display/simview.cc \
	$(srcdir)/gui/ai_option_t.cc \
	$(srcdir)/gui/banner.cc \
	$(srcdir)/gui/building_info.cc \
	$(srcdir)/gui/city_info.cc \
	$(srcdir)/gui/citylist_frame_t.cc \
	$(srcdir)/gui/citylist_stats_t.cc \
	$(srcdir)/gui/components/gui_button.cc \
	$(srcdir)/gui/components/gui_chart.cc \
	$(srcdir)/gui/components/gui_colorbox.cc \
	$(srcdir)/gui/components/gui_combobox.cc \
	$(srcdir)/gui/components/gui_convoiinfo.cc \
	$(srcdir)/gui/components/gui_convoy_assembler.cc \
	$(srcdir)/gui/components/gui_convoy_formation.cc \
	$(srcdir)/gui/components/gui_convoy_label.cc \
	$(srcdir)/gui/components/gui_convoy_payloadinfo.cc \
	$(srcdir)/gui/components/gui_divider.cc \
	$(srcdir)/gui/components/gui_factory_storage_info.cc \
	$(srcdir)/gui/components/gui_fixedwidth_textarea.cc \
	$(srcdir)/gui/components/gui_flowtext.cc \
	$(srcdir)/gui/components/gui_halthandled_lines.cc \
	$(srcdir)/gui/components/gui_label.cc \
	$(srcdir)/gui/components/gui_line_lettercode.cc \
	$(srcdir)/gui/components/gui_numberinput.cc \
	$(srcdir)/gui/components/gui_schedule_item.cc \
	$(srcdir)/gui/components/gui_scrollbar.cc \
	$(srcdir)/gui/components/gui_scrolled_list.cc \
	$(srcdir)/gui/components/gui_tab_panel.cc \
	$(srcdir)/gui/components/gui_textarea.cc \
	$(srcdir)/gui/components/gui_textinput.cc \
	$(srcdir)/gui/components/gui_vehicle_capacitybar.cc \
	$(srcdir)/gui/convoi_detail_t.cc \
	$(srcdir)/gui/convoi_info_t.cc \
	$(srcdir)/gui/curiositylist_frame_t.cc \
	$(srcdir)/gui/depot_frame.cc \
	$(srcdir)/gui/depotlist_frame.cc \
	$(srcdir)/gui/display_settings.cc \
	$(srcdir)/gui/fabrik_info.cc \
	$(srcdir)/gui/factory_chart.cc \
	$(srcdir)/gui/factory_legend.cc \
	$(srcdir)/gui/factorylist_frame_t.cc \
	$(srcdir)/gui/factorylist_stats_t.cc \
	$(srcdir)/gui/goods_frame_t.cc \
	$(srcdir)/gui/goods_stats_t.cc \
	$(srcdir)/gui/gui_theme.cc \
	$(srcdir)/gui/halt_detail.cc \
	$(srcdir)/gui/halt_info.cc \
	$(srcdir)/gui/halt_list_filter_frame.cc \
	$(srcdir)/gui/halt_list_stats.cc \
	$(srcdir)/gui/labellist_frame_t.cc \
	$(srcdir)/gui/line_color_gui.cc \
	$(srcdir)/gui/line_item.cc \
	$(srcdir)/gui/loadfont_frame.cc \
	$(srcdir)/gui/minimap.cc \
	$(srcdir)/gui/money_frame.cc \
	$(srcdir)/gui/player_frame_t.cc \
	$(srcdir)/gui/replace_frame.cc \
	$(srcdir)/gui/schedule_gui.cc \
	$(srcdir)/gui/schedule_list.cc \
	$(srcdir)/gui/signalboxlist_frame.cc \
	$(srcdir)/gui/simwin.cc \
	$(srcdir)/gui/slim_obj_info.cc \
	$(srcdir)/gui/times_history_container.cc \
	$(srcdir)/gui/tool_selector.cc \
	$(srcdir)/gui/vehiclelist_frame.cc \
	$(srcdir)/gui/water_info.cc \
	$(srcdir)/gui/way_info.cc \
	$(srcdir)/network/network.cc \
	$(srcdir)/obj/zeiger.cc \
	$(srcdir)/simloadingscreen.cc \
	$(srcdir)/simmain.cc \
	$(srcdir)/simticker.cc \
	$(srcdir)/vehicle/vehicle.cc \


.PHONY: all clean

all: $(BUILDDIR)/simutrans-nettool $(BUILDDIR)/simutrans-makeobj $(BUILDDIR)/simutrans-server $(BUILDDIR)/simutrans-sdl2mix

.patched:
	$(QUILT_CMD) push -a
	touch '$@'

clean:
	$(QUILT_CMD) pop -a
	$(RM) -r '$(BUILDDIR)' .patched

$(BUILDDIR):
	mkdir -p '$(BUILDDIR)'

$(BUILDDIR)/libsimutrans-squirrel.so: \
	$(srcdir)/squirrel/sq_extensions.cc \
	$(srcdir)/squirrel/sqstdlib/sqstdaux.cc \
	$(srcdir)/squirrel/sqstdlib/sqstdblob.cc \
	$(srcdir)/squirrel/sqstdlib/sqstdio.cc \
	$(srcdir)/squirrel/sqstdlib/sqstdmath.cc \
	$(srcdir)/squirrel/sqstdlib/sqstdrex.cc \
	$(srcdir)/squirrel/sqstdlib/sqstdstream.cc \
	$(srcdir)/squirrel/sqstdlib/sqstdstring.cc \
	$(srcdir)/squirrel/sqstdlib/sqstdsystem.cc \
	$(srcdir)/squirrel/squirrel/sqapi.cc \
	$(srcdir)/squirrel/squirrel/sqbaselib.cc \
	$(srcdir)/squirrel/squirrel/sqclass.cc \
	$(srcdir)/squirrel/squirrel/sqcompiler.cc \
	$(srcdir)/squirrel/squirrel/sqdebug.cc \
	$(srcdir)/squirrel/squirrel/sqfuncstate.cc \
	$(srcdir)/squirrel/squirrel/sqlexer.cc \
	$(srcdir)/squirrel/squirrel/sqmem.cc \
	$(srcdir)/squirrel/squirrel/sqobject.cc \
	$(srcdir)/squirrel/squirrel/sqstate.cc \
	$(srcdir)/squirrel/squirrel/sqtable.cc \
	$(srcdir)/squirrel/squirrel/sqvm.cc \
	.patched | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) $(SOFLAGS)
	strip -s '$@'

# symbols shared by the server, clients, makeobj and nettool
SOURCES_CORE +=	\
	$(srcdir)/dataobj/freelist.cc \
	$(srcdir)/simdebug.cc \
	$(srcdir)/simmem.cc \
	$(srcdir)/utils/simstring.cc

# symbols shared by the server, clients, and makeobj
SOURCES_OBJ += \
	$(srcdir)/utils/cbuffer_t.cc \
	$(srcdir)/utils/csv.cc \
	$(srcdir)/utils/searchfolder.cc
	
# symbols shared by the server, clients, and nettool
SOURCES_NET +=	\
	$(srcdir)/network/memory_rw.cc \
	$(srcdir)/network/network_address.cc \
	$(srcdir)/network/network_packet.cc \
	$(srcdir)/network/pwd_hash.cc \
	$(srcdir)/utils/sha1.cc

# for symbols required by the server, clients, and (makeobj or nettool)
$(BUILDDIR)/libsimutrans-core.so: .patched $(SOURCES_CORE) | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) $(SOFLAGS)
	$(STRIP_CMD)

$(BUILDDIR)/libsimutrans-net.so: .patched $(SOURCES_NET) | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) $(SOFLAGS)
	$(STRIP_CMD)

$(BUILDDIR)/libsimutrans-obj.so: .patched $(SOURCES_OBJ) | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) $(SOFLAGS)
	$(STRIP_CMD)

# for symbols required by the dedicated server and all GUI clients
$(BUILDDIR)/libsimutrans-base.so: \
	$(BUILDDIR)/libsimutrans-core.so \
	$(BUILDDIR)/libsimutrans-net.so \
	$(BUILDDIR)/libsimutrans-obj.so \
	$(BUILDDIR)/libsimutrans-squirrel.so \
	$(SOURCES_BASE) .patched | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) $(SOFLAGS) -lsimutrans-core -lsimutrans-net -lsimutrans-obj -lsimutrans-squirrel -lbz2 -lzstd -lz
	$(STRIP_CMD)

# for symbols required by all GUI clients
$(BUILDDIR)/libsimutrans-gfx16.so: \
	$(srcdir)/io/raw_image_bmp.cc \
	$(srcdir)/io/raw_image.cc \
	$(srcdir)/io/raw_image_png.cc \
	$(srcdir)/io/raw_image_ppm.cc \
	.patched $(SOURCES_GFX) | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) $(SOFLAGS) -DCOLOUR_DEPTH=16 -DUSE_FREETYPE -I/usr/include/freetype2 -lfreetype -lminiupnpc
	$(STRIP_CMD)

$(BUILDDIR)/simutrans-makeobj: \
	$(BUILDDIR)/libsimutrans-core.so \
	$(BUILDDIR)/libsimutrans-obj.so \
	$(srcdir)/dataobj/tabfile.cc \
	$(srcdir)/descriptor/image.cc \
	$(srcdir)/descriptor/writer/bridge_writer.cc \
	$(srcdir)/descriptor/writer/building_writer.cc \
	$(srcdir)/descriptor/writer/citycar_writer.cc \
	$(srcdir)/descriptor/writer/crossing_writer.cc \
	$(srcdir)/descriptor/writer/factory_writer.cc \
	$(srcdir)/descriptor/writer/get_climate.cc \
	$(srcdir)/descriptor/writer/get_waytype.cc \
	$(srcdir)/descriptor/writer/good_writer.cc \
	$(srcdir)/descriptor/writer/groundobj_writer.cc \
	$(srcdir)/descriptor/writer/ground_writer.cc \
	$(srcdir)/descriptor/writer/imagelist2d_writer.cc \
	$(srcdir)/descriptor/writer/imagelist3d_writer.cc \
	$(srcdir)/descriptor/writer/imagelist_writer.cc \
	$(srcdir)/descriptor/writer/image_writer.cc \
	$(srcdir)/descriptor/writer/obj_node.cc \
	$(srcdir)/descriptor/writer/obj_writer.cc \
	$(srcdir)/descriptor/writer/pedestrian_writer.cc \
	$(srcdir)/descriptor/writer/pier_writer.cc \
	$(srcdir)/descriptor/writer/roadsign_writer.cc \
	$(srcdir)/descriptor/writer/root_writer.cc \
	$(srcdir)/descriptor/writer/sim_writer.cc \
	$(srcdir)/descriptor/writer/skin_writer.cc \
	$(srcdir)/descriptor/writer/sound_writer.cc \
	$(srcdir)/descriptor/writer/text_writer.cc \
	$(srcdir)/descriptor/writer/tree_writer.cc \
	$(srcdir)/descriptor/writer/tunnel_writer.cc \
	$(srcdir)/descriptor/writer/vehicle_writer.cc \
	$(srcdir)/descriptor/writer/way_obj_writer.cc \
	$(srcdir)/descriptor/writer/way_writer.cc \
	$(srcdir)/descriptor/writer/xref_writer.cc \
	$(srcdir)/makeobj/makeobj.cc \
	$(srcdir)/utils/dr_rdpng.cc \
	$(srcdir)/utils/float32e8_t.cc \
	$(srcdir)/utils/log.cc \
	$(srcdir)/utils/simrandom.cc \
	.patched | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) -DCOLOUR_DEPTH=0 -DMAKEOBJ=1 -lsimutrans-core -lsimutrans-obj -lpng
	$(STRIP_CMD)

$(BUILDDIR)/simutrans-nettool: \
	$(BUILDDIR)/libsimutrans-core.so \
	$(BUILDDIR)/libsimutrans-net.so \
	$(srcdir)/nettools/nettool.cc \
	$(srcdir)/network/network.cc \
	$(srcdir)/network/network_cmd.cc \
	$(srcdir)/network/network_file_transfer.cc \
	$(srcdir)/network/network_socket_list.cc \
	$(srcdir)/utils/fetchopt.cc \
	$(srcdir)/utils/log.cc \
	.patched | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) -DCOLOUR_DEPTH=0 -DNETTOOL=1 -lsimutrans-core -lsimutrans-net
	$(STRIP_CMD)

$(BUILDDIR)/simutrans-server: \
	$(BUILDDIR)/libsimutrans-base.so \
	$(SOURCES_GFX) \
	$(srcdir)/display/simgraph0.cc \
	$(srcdir)/music/no_midi.cc \
	$(srcdir)/sound/no_sound.cc \
	$(srcdir)/sys/simsys_posix.cc \
	.patched | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) -DCOLOUR_DEPTH=0 -lsimutrans-base -lminiupnpc
	$(STRIP_CMD)

$(BUILDDIR)/simutrans-sdl2mix: \
	$(BUILDDIR)/libsimutrans-base.so \
	$(BUILDDIR)/libsimutrans-gfx16.so \
	$(srcdir)/display/simgraph16.cc \
	$(srcdir)/music/sdl2_mixer_midi.cc \
	$(srcdir)/sound/sdl2_mixer_sound.cc \
	$(srcdir)/sys/simsys_s2.cc \
	.patched | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -o '$@' $^ $(LTOFLAGS) -DCOLOUR_DEPTH=16 -I/usr/include/SDL2 -lsimutrans-base -lsimutrans-gfx16 -lSDL2 -lSDL2_mixer
	$(STRIP_CMD)

