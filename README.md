Hewwo!

This is a custom fork of the wonderful game Simutrans Extended, which is itself a fork of the original Simutrans.

This fork is mostly just customizations for myself, but of course anyone is allowed to play (or copy!). Here's some of the goals I had in mind:
* Be save-game and multiplayer compatible with upstream Simutrans Extended. (IMPORTANT!)
* Use Quilt for patch management like all the cool people.
* Utilize shared objects (.so) to share code between makeobj, nettool, the server, and all the client(s).
* Add an X11 icon (done!) so that the application has a visible icon even without XDG integration.
* Set an X11 application name (similar to icon). This is done via the "SDL_VIDEO_X11_WMCLASS" variable.
* Overhaul the command line argument parsing to be more like other GNU commands.
    * Update the --help message (TODO)
    * Use double dashes.
    * Use absolute paths (TODO)
* Slim down the start up banner
* Remove all log file and syslog code. Logging should be output to stdout and stderr only. Logging to file should be accomplished with redirects, and syslog logging should be done with the logger utility.
* Probably only compiles on Linux currently (this isn't intentional, I'd love to have it working on all platforms again!)
